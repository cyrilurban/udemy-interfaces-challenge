package com.urban;

import java.util.ArrayList;

public interface ISaveable {
    void saveData(String data);
    ArrayList readData();
}
