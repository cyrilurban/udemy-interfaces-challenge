package com.urban;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//        ISaveable storage = new FlashDisc();
        ISaveable storage = new DVD();

        Scanner scanner = new Scanner(System.in);
        boolean quit = false;

        System.out.println("Choose\n" +
                "2 read data from storage\n" +
                "1 to enter a string\n" +
                "0 to quit");

        while (!quit) {
            System.out.print("Choose an option: ");
            int choice = scanner.nextInt();
            scanner.nextLine();
            switch (choice) {
                case 0:
                    quit = true;
                    break;
                case 1:
                    System.out.print("Enter a string: ");
                    String stringInput = scanner.nextLine();
                    storage.saveData(stringInput);
                    break;
                case 2:
                    System.out.println(storage.readData());
                    break;
            }
        }
    }
}
