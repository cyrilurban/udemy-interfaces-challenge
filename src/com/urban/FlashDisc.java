package com.urban;

import java.util.ArrayList;

public class FlashDisc implements ISaveable {
    private ArrayList data;

    public FlashDisc() {
        this.data = new ArrayList();
    }

    @Override
    public void saveData(String data) {
        System.out.println("Saving data to the Flash Disc");
        this.data.add(data);
    }

    @Override
    public ArrayList readData() {
        System.out.println("Reading data from the Flash Disc");
        return this.data;
    }

    @Override
    public String toString() {
        String text = "";
        for(int i = 0; i < this.data.size(); i++) {
            text += this.data.get(i) + "\n";
        }
        return text;
    }
}
