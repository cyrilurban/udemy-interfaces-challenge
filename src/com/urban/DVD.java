package com.urban;

import java.util.ArrayList;

public class DVD implements ISaveable {
    private ArrayList data;

    public DVD() {
        this.data = new ArrayList();
    }

    @Override
    public void saveData(String data) {
        System.out.println("Saving data on the DVD");
        this.data.add(data);
    }

    @Override
    public ArrayList readData() {
        System.out.println("Reading data from the DVD");
        return this.data;
    }

    @Override
    public String toString() {
        String text = "";
        for(int i = 0; i < this.data.size(); i++) {
            text += this.data.get(i) + "\n";
        }
        return text;
    }
}
